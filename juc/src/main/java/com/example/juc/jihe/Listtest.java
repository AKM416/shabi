package com.example.juc.jihe;

import com.example.juc.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Listtest {
    public static void main(String[] args) {
        Listtest listtest=new Listtest();
        List<User> users=listtest.add();



        users=users.stream().filter(user -> {
            return user.getSex().equals("男");
        }).collect(Collectors.toList());

        users.forEach(user -> {
            System.out.println(user.getId()+"==>"+user.getName()+
                    "==>"+user.getAge()+"==>"+user.getSex());
        });
    }

    public void list(){
        List<String> list=new ArrayList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        for (int i = 0; i <list.size() ; i++) {
            if(i==1){
                list.remove(i);
            }

        }

        list.forEach(e ->{
            System.out.println(e);
        });
    }


    public List<User> add(){
        List<User> users=new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            if(i%2==0){
                User user=new User(i,"xiongMou",i+10,"男");
                users.add(user);
            }else {
                User user=new User(i,"熊某",i+10,"女");
                users.add(user);
            }
        }

        return users;
    }

}
