package com.example.juc.single;

/**
 * 懒汉单例模式
 */
public class LanSingleton {
    private LanSingleton(){
        System.out.println("线程"+Thread.currentThread().getName()+"创建了对象");
    }
    private volatile static LanSingleton lanSingleton;


   /* public synchronized static LanSingleton getInstance(){
        if(lanSingleton==null) {
            lanSingleton= new LanSingleton();
        }
        return lanSingleton;
    }*/
    public static LanSingleton getInstance(){
        if(lanSingleton==null) {
            synchronized (LanSingleton.class){
                if(lanSingleton==null){
                    lanSingleton= new LanSingleton();
                }
            }
        }
        return lanSingleton;
    }
}
