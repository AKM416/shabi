package com.example.juc.single;

public class ESingleton {
    private ESingleton(){

    }

    private static ESingleton getInstance=new ESingleton();

    public static ESingleton getInstance(){
        return getInstance;
    }
}
