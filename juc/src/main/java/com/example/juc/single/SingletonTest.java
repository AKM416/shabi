package com.example.juc.single;

public class SingletonTest {
    public static void main(String[] args) {
        for (int i = 0; i <10; i++) {
            new Thread(()->{
                LanSingleton.getInstance();
            },String.valueOf(i+1)).start();
        }
    }
}
