package com.example.juc.juctest;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Test1 {
    public static void main(String[] args) {
        TakeTick takeTick=new TakeTick();

        //(参数)->{ 代码 })
       /* new Thread(new Runnable() {
            @Override
            public void run() {

            }
        }).start();*/
        new Thread(()->{
            for(int i=0;i<=30;i++){
                takeTick.take();
            }
        },"A").start();
        new Thread(()->{
            for(int i=0;i<=30;i++){
                takeTick.take();
            }
        },"B").start();
        new Thread(()->{
            for(int i=0;i<=30;i++){
                takeTick.take();
            }
        },"C").start();



    }
}
class TakeTick{
    private int num=50;
    private int count=0;

    //不公平锁
    Lock lock=new ReentrantLock();
    //公平锁
//    Lock lock=new ReentrantLock(true);
    public void take(){
        lock.lock();
        try {
            while (num>0) {
                System.out.println("线程" + Thread.currentThread().getName() + "卖出第" + (count++) + "张,还剩" + (num--) + "张");
            }
        }finally {
            lock.unlock();
        }

    }
}
